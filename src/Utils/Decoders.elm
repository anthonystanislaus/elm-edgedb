module Utils.Decoders exposing (DBResponse(..), Transfer, responseDecoder)

import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder, field, list, string)



-- TYPES


type DBResponse
    = DBTransfers (List Transfer)
    | DBUsers (List User)
    | DBError ErrorPayload


type alias User =
    { name : String
    }


type alias Transfer =
    { title : String
    , description : String
    , amount : Float
    , from_account : User
    , to_account : User
    , document_date : String
    }


type alias ErrorPayload =
    { message : String
    , type_ : String
    , code : String
    }


type alias DBRequest =
    { query : String
    , variables : Dict String String
    }



-- DECODERS


usersDecoder : Decoder DBResponse
usersDecoder =
    Decode.map DBUsers (list userDecoder)


userDecoder : Decoder User
userDecoder =
    Decode.map User
        (field "name" string)


transfersDecoder : Decoder DBResponse
transfersDecoder =
    Decode.map DBTransfers (list transferDecoder)


transferDecoder : Decoder Transfer
transferDecoder =
    Decode.map6 Transfer
        (field "title" string)
        (field "description" string)
        (field "amount" stringToFloatDecoder)
        (field "from_account" userDecoder)
        (field "to_account" userDecoder)
        (field "document_date" string)


errorPayloadDecoder : Decoder ErrorPayload
errorPayloadDecoder =
    Decode.map3 ErrorPayload
        (field "message" string)
        (field "type" string)
        (field "code" string)


dataDecoder : Decoder DBResponse
dataDecoder =
    Decode.at [ "data" ] (Decode.oneOf [ transfersDecoder, usersDecoder ])


errorDecoder : Decoder DBResponse
errorDecoder =
    Decode.at [ "error" ] (Decode.map DBError errorPayloadDecoder)


unexpectedDecoder : Decoder DBResponse
unexpectedDecoder =
    Decode.fail "Unexpected top level field: consider adding a field handler."


responseDecoder : Decoder DBResponse
responseDecoder =
    Decode.oneOf [ dataDecoder, errorDecoder, unexpectedDecoder ]



-- HELPERS


stringToFloatDecoder =
    string
        |> Decode.andThen
            (\value ->
                case String.toFloat value of
                    Just a ->
                        Decode.succeed a

                    Nothing ->
                        Decode.fail "Parse error: unable to parse String to Float."
            )



{--
    Request
    {
      "query": "insert Person { name := <str>$name };",
      "variables": { "name": "Pat" }
    }
    Response
    {
      "data": [ ... ]
      OR
      "error": {
        "message": "Error message",
        "type": "ErrorType",
        "code": 123456
      }
    }
    Transfer
    {
        "title": "transaction_2",
        "description": "Transaction 2",
        "amount": 71532,
        "fromAccount": "account_4",
        "toAccount": "account_6",
        "transactionDate": "2023-04-15"
    }
--}
