module Main exposing (Model, Msg(..), init, main, subscriptions, update, view)

import Browser exposing (Document, UrlRequest(..), application)
import Browser.Navigation as Nav
import Html exposing (Html, a, article, div, h2, h3, p, span, text, time)
import Html.Attributes exposing (class, datetime, href)
import Http exposing (Error, expectJson, jsonBody)
import Json.Encode as Encode exposing (Value)
import Url exposing (Url)
import Utils.Decoders exposing (DBResponse(..), Transfer, responseDecoder)



-- TYPES


type Msg
    = UrlRequested UrlRequest
    | UrlChanged Url
    | GotResponse (Result Error DBResponse)


type alias Model =
    { url : Url
    , key : Nav.Key
    , transfers : Maybe (List Transfer)
    }



-- MODEL


init : Float -> Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( Model url key Nothing, fetchAllTransfers )



-- VIEW


view : Model -> Document Msg
view { transfers } =
    { title = "537D"
    , body =
        [ div
            [ class "h-screen w-full font-primary p-5 grid" ]
            [ transfersList transfers ]
        ]
    }


transfersList : Maybe (List Transfer) -> Html Msg
transfersList transfers =
    div [ class "mx-auto max-w-7xl px-6 lg:px-8" ]
        [ h2 [ class "text-center text-3xl font-bold tracking-tight text-neutral-700 sm:text-4xl" ]
            [ text "Transactions" ]
        , div [ class "mx-auto mt-10 grid max-w-2xl grid-cols-2 gap-x-8 gap-y-4 border-t pt-10 border-neutral-200 sm:mt-16 sm:pt-16 lg:mx-0 lg:max-w-none lg:grid-cols-3" ]
            (case transfers of
                Just a ->
                    List.map transferCard a

                Nothing ->
                    []
            )
        ]


transferCard : Transfer -> Html Msg
transferCard transfer =
    article [ class "border-2 border-neutral-50 rounded p-4 flex max-w-xl flex-col items-start justify-between" ]
        [ div [ class "w-full flex justify-between items-center gap-x-4 text-xs" ]
            [ time [ datetime "2020-03-16", class "text-neutral-500" ] [ text transfer.document_date ]
            , a [ href "#", class "rounded-full bg-neutral-50 px-3 py-1.5 font-medium text-neutral-600 hover:bg-neutral-100" ]
                [ text <| String.concat [ transfer.to_account.name, " $", String.fromFloat transfer.amount ] ]
            ]
        , div [ class "group" ]
            [ h3 [ class "mt-3 text-md font-semibold leading-6 text-neutral-850 group-hover:text-neutral-600" ]
                [ a [ href "#" ]
                    [ span [] [ text transfer.title ] ]
                ]
            , p [ class "mt-5 line-clamp-3 text-xs leading-6 text-neutral-600" ]
                [ text transfer.description
                ]
            ]
        ]



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlRequested urlRequest ->
            case urlRequest of
                Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | url = url }
            , Cmd.none
            )

        GotResponse result ->
            ( handleGotResponse model result, Cmd.none )


handleGotResponse : Model -> Result Error DBResponse -> Model
handleGotResponse model result =
    case result of
        Ok value ->
            case value of
                DBTransfers transfers ->
                    { model | transfers = Just transfers }

                DBUsers _ ->
                    model

                DBError _ ->
                    model

        Err error ->
            let
                _ =
                    Debug.log "RESPONSE" error
            in
            model



-- REQUESTS


fetchAllTransfers : Cmd Msg
fetchAllTransfers =
    Http.get
        { url = "http://localhost:8080/api/transfers"
        , expect = expectJson GotResponse responseDecoder
        }


fetchTransfers : String -> String -> Cmd Msg
fetchTransfers object properties =
    Http.post
        { url = "http://localhost:8080/api/transfers"
        , body = jsonBody (select object properties)
        , expect = expectJson GotResponse responseDecoder
        }



-- ENCODERS


select : String -> String -> Value
select object properties =
    let
        query =
            String.concat [ "select ", object, " {", properties, "};" ]
    in
    Encode.object [ ( "query", Encode.string query ) ]



-- MAIN


main : Program Float Model Msg
main =
    application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = UrlRequested
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
