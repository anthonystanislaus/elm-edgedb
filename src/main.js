import { Elm } from "./Main.elm";
import "./assets/index.css";


const root = document.querySelector("#app");
Elm.Main.init({
  node: root,
  flags: 2.5,
});
