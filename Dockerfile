# Production Stage
FROM node:20-alpine as prod-stage
LABEL authors="anthony"

WORKDIR /app

COPY . .

RUN npm install -g pnpm
RUN pnpm install
RUN pnpm prebuild
RUN pnpm build

EXPOSE 3000

CMD ["pnpm", "serve"]