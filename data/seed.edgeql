INSERT User {
  name := "Anthony"
};

INSERT User {
  name := "Enzo"
};

INSERT User {
  name := "Barry"
};

INSERT Transfer {
  title := "Test Transfer One",
  description := "My first test transfer to the EdgeDB instance.",
  amount := <decimal>71532.00,
  from_account := (select User filter .name = "Anthony"),
  to_account := (select User filter .name = "Enzo")
};

INSERT Transfer {
  title := "Test Transfer Two",
  description := "My second test transfer to the EdgeDB instance.",
  amount := <decimal>12.58,
  from_account := (select User filter .name = "Anthony"),
  to_account := (select User filter .name = "Enzo")
};

INSERT Transfer {
  title := "Test Transfer Three",
  description := "My three test transfer to the EdgeDB instance.",
  amount := <decimal>14785.00,
  from_account := (select User filter .name = "Enzo"),
  to_account := (select User filter .name = "Anthony")
};

INSERT Transfer {
  title := "Test Transfer Four",
  description := "My four test transfer to the EdgeDB instance.",
  amount := <decimal>9435.73,
  from_account := (select User filter .name = "Enzo"),
  to_account := (select User filter .name = "Anthony")
};