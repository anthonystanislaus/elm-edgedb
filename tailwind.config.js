/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: [
      "./index.html",
      "./src/**/*.{elm,ts,js,html}"
  ],
  theme: {
    extend: {
      fontFamily: {
        primary: [ "Space Grotesk", "Inconsolata", "Ligconsolata", "Consolas", "Arial" ],
        secondary: [ "'JetBrains Mono'" ]
      },
    },
  },
  plugins: [],
  variants: {},
}
