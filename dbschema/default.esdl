module default {
  # ---------------------------------------------------------------------
  # ABSTRACT TYPES ------------------------------------------------------
  # ---------------------------------------------------------------------
  abstract type Account {
    required name: str {
      delegated constraint exclusive;
      delegated constraint min_len_value(1);
      delegated constraint max_len_value(45);
    }
    property creation_date: datetime {
      default := datetime_of_statement();
    }
    property last_updated: datetime {
      rewrite insert, update using (datetime_of_statement());
    }
  }

  abstract type Operation {
    required title: str {
      delegated constraint min_len_value(1);
      delegated constraint max_len_value(45);
    }
    required description: str {
      delegated constraint min_len_value(1);
    }
  }

  # ---------------------------------------------------------------------
  # CONCRETE TYPES ------------------------------------------------------
  # ---------------------------------------------------------------------
  type User extending Account;

  type Transfer extending Operation {
    required amount: decimal;
    required from_account: Account;
    required to_account: Account;
    required transaction_date: datetime {
      default := datetime_of_statement();
    }
  }
}
