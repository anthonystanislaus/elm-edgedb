CREATE MIGRATION m1qitdmmhb7btx7ttxrpfheyrgqtde2ezvjwfxz3eqasel6xixp7bq
    ONTO initial
{
  CREATE ABSTRACT TYPE default::Account {
      CREATE PROPERTY creation_date: std::datetime {
          SET default := (std::datetime_of_statement());
      };
      CREATE PROPERTY last_updated: std::datetime {
          CREATE REWRITE
              INSERT 
              USING (std::datetime_of_statement());
          CREATE REWRITE
              UPDATE 
              USING (std::datetime_of_statement());
      };
      CREATE REQUIRED PROPERTY name: std::str {
          CREATE DELEGATED CONSTRAINT std::exclusive;
          CREATE DELEGATED CONSTRAINT std::max_len_value(45);
          CREATE DELEGATED CONSTRAINT std::min_len_value(1);
      };
  };
  CREATE TYPE default::User EXTENDING default::Account;
  CREATE ABSTRACT TYPE default::Operation {
      CREATE REQUIRED PROPERTY description: std::str {
          CREATE DELEGATED CONSTRAINT std::min_len_value(1);
      };
      CREATE REQUIRED PROPERTY title: std::str {
          CREATE DELEGATED CONSTRAINT std::max_len_value(45);
          CREATE DELEGATED CONSTRAINT std::min_len_value(1);
      };
  };
  CREATE TYPE default::Transfer EXTENDING default::Operation {
      CREATE REQUIRED LINK from_account: default::Account;
      CREATE REQUIRED LINK to_account: default::Account;
      CREATE REQUIRED PROPERTY amount: std::decimal;
      CREATE REQUIRED PROPERTY transaction_date: std::datetime {
          SET default := (std::datetime_of_statement());
      };
  };
};
