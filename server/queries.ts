import e from "../dbschema/edgeql-js";
import { Expression } from "../dbschema/edgeql-js/typesystem.ts";


function fetchAllTransfers(): Expression {
  return e.select(e.Transfer, (transfer) => ({
    title: true,
    description: true,
    from_account: { name: true },
    to_account: { name: true },
    amount: true,
    // transaction_date: true,  // unable to redefine scalar properties, using "occurred_on"
    document_date: e.to_str(transfer.transaction_date, "FMMon FMDDth, YYYY"),
    order_by: transfer.transaction_date
  }));
}

export default {
  fetchAllTransfers
};
