import server from "bunrest";
import { createClient } from "edgedb";
import Query from "./queries.ts";
import { Transfer } from "../dbschema/interfaces.ts";
import { BunRequest } from "bunrest/src/server/request.ts";
import { BunResponse } from "bunrest/src/server/response.ts";


const app = server();
app.use(handleCORS);

const router = app.router();
const ALLOWED_ORIGINS = ["http://localhost:3000", "http://localhost:8080"];
const client = createClient();

router.get("/", async (req, res) => {
  res.status(200).json({ message: "Jello, World!" });
});

router.get("/transfers", async (req, res) => {
  const transfers: Transfer[] = await Query.fetchAllTransfers().run(client);
  res.status(200).json({ "data": transfers });
});

app.use("/api", router);

app.listen(8080, () => {
  console.log("Server running on port: 8080");
});

async function handleCORS(request: BunRequest, response: BunResponse, next: any) {
  response.setHeader("Access-Control-Allow-Origin", "*");
  next();
  // const origin = request.headers?.origin;
  // if (ALLOWED_ORIGINS.includes(origin)) {
  //   response.setHeader("Access-Control-Allow-Origin", origin);
  //   next();
  // } else {
  //   response.status(401).json({ error: "Unauthorized" });
  // }
}
