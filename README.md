# Transactions Takehome Template


<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
    </li>
  </ol>
</details>


## About The Project

Trying out a "backendless" application using:
- Elm (Frontend)
- EdgeDB (Auth, Database, Computed properties)

Copied from: [Heard technical assessment](https://github.com/Heard-Mental-Health/heard-technical-assessment)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


### Built With

* [![Elm][Elm.lang]][Elm-url]
* [EdgeDB][EdgeDB-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>


## Prerequisites

- Docker


## Getting Started

Using docker compose:
```zsh
# requires Docker
pnpm up:prod

# connect to EdgeDB instance
docker exec -it edgedb bash
cd database/
edgedb --tls-security=insecure -P 5656
```


<!--
Using Gitpod:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/anthonystanislaus/elm-edgedb)

after Gitpod startup:
```zsh
pnpm install
pnpm postinstall
pnpm serve  # Gitpod will notify with URL
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>
-->

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[Elm.lang]: https://img.shields.io/badge/Elm-60B5CC?style=for-the-badge&logo=elm&logoColor=white
[Elm-url]: https://elm-lang.org
[EdgeDB-url]: https://www.edgedb.com
